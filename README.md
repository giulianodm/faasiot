# IBM Cloud Functions - IoT + Whatsapp

[![N|Solid](https://developer.ibm.com/code/wp-content/uploads/sites/118/2017/10/IBM-CLOUD-FUNCTIONS-35.png)](https://nodesource.com/products/nsolid)



This project is a quick demonstration usinereg IBM Cloud Functions with an IoT use case

![N|Solid](https://preview.ibb.co/eZ4uJA/gitlab-usecase.jpg)
    
The final architecture is represented here

![N|Solid](https://preview.ibb.co/jAJzkq/gitlab-architecture.jpg)

# How it works!

  - A sensortag from Texas instruments was used to read temperature data from any machine, in this demonstration, and industrial oven was considered
  - The sensortag uses bluetooth to connect to a SensorTag App, which is configured to send data to the Watson IoT Platform running in the IBM Cloud
  - Once the collected sensor data arrives at the Watson IoT Platform, it is then moved to a topic in the IBM Event Streams (also known as MessageHub - a kafka base service running in the IBM Cloud)
  - This data transfer is needed because IBM Cloud Functions has a built in trigger that starts an action when data arrives in a specified topic in the kafka service
  - The trigger starts a sequence (chain of actions) that includes going out to the weather service to check the temperature and humidity of the location (city) where the sensor is. Next with these data in hand it will calculate the heat index. If the heat index is above a certain limit it will generate an alarm through the Twillio whatsapp service
  

![N|Solid](https://preview.ibb.co/hK7gyA/gitlab-mobile.jpg)

![N|Solid](https://preview.ibb.co/bGmkQq/gitlab-watson-iot.jpg)

![N|Solid](https://preview.ibb.co/de18dA/gitlab-sensors.jpg)

![N|Solid](https://preview.ibb.co/de18dA/gitlab-sensors.jpg)

![N|Solid](https://preview.ibb.co/gifekq/gitlab-messagehub.jpg)

![N|Solid](https://preview.ibb.co/gnHQQq/gitlab-messagehub2.jpg)

![N|Solid](https://preview.ibb.co/gnHQQq/gitlab-messagehub2.jpg)

![N|Solid](https://preview.ibb.co/dEnLrV/gitlab-sequence.jpg)



IBM Cloud Functions

