
var request = require("request");

function main(params) {
    var location = 'Santos,BR';   
    var sensortemp = params.messages[0].value.d.ambientTemp;
    console.log("Temp sensor: "+ params.messages[0].value.d.ambientTemp);
    var keyObj = JSON.parse(params.messages[0].key);
    console.log(keyObj.deviceId);
    if (keyObj.deviceId == "giulianosensor1") {
        location = 'Curitiba,BR';       
    }
    console.log(location);
    
    var url = "https://query.yahooapis.com/v1/public/yql?q=select item.condition, atmosphere.humidity from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + location + "') and u='c'&format=json"

    return new Promise(function(resolve, reject) {
        request.get(url, function(error, response, body) {
            if (error) {
                reject(error);
            }
            else {
                var condition = JSON.parse(body).query.results.channel.item.condition;
                var humidity =  JSON.parse(body).query.results.channel.atmosphere.humidity;
                var text = condition.text;
                var temperature = condition.temp;
                
                var dados = {
                    T: temperature,
                    R: humidity,
                    sensor: sensortemp
                }
                resolve({dados});
            }
        });
    }).then(function (result) {
        console.log('deu certo'); // "Stuff worked!"
        var T = result.dados.T;
        var R = result.dados.R;
        var tempiot = result.dados.sensor;
        console.log(T);
        var heatindex = heatIt(T,R);
        console.log(heatindex);
        console.log(checkTempIsOver(tempiot,heatindex, 15));
        var mensagem = null;
        if (checkTempIsOver(tempiot,heatindex, 15)) {
            mensagem = "The heat index for the sensor location combined with device temperature might cause damage. Device temp is "+ tempiot +" and heat index is " + heatindex;
        } else {
            mensagem = null;
        }
        return { message: mensagem};
    });
    
    
}

function heatIt(temp, rh) {
    var temp = temp*(9/5) + 32
    var t2=temp*temp;
    var t3=t2*temp;
    var rh2=rh*rh;
    var rh3=rh2*rh;
    var index =16.923+0.185212*temp+5.37941*rh-0.100254*temp*rh+ 0.941695e-2*t2+0.728898e-2*rh2+0.345372e-3*t2*rh- 0.814971e-3*temp*rh2+0.102102e-4*t2*rh2- 0.38646e-4*t3+0.291583e-4*rh3+0.142721e-5*t3*rh+ 0.197483e-6*temp*rh3-0.218429e-7*t3*rh2+ 0.843296e-9*t2*rh3-0.481975e-10*t3*rh3;
    var index = (index-32)*(5/9)
    var heatindex = parseInt(index);
    return heatindex; // - 10
}

//check if heat index is xlimit% over the sensor (oven) temperature. If it is, we should fire an alarm
function checkTempIsOver(current, heatindex, overlimit) {
    var overcalc = ((heatindex / current) - 1) * 100;
    return overcalc >= overlimit;
}

